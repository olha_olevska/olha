package homework_17;

public class User {

	private String firstName;
	private String surname;
	private String email;
	private String mobilePhone;
	private String workPhone;
	private String password;

	public User(String firstName, String surname, String email, String mobilePhone, String workPhone, String password) {
		
		this.firstName = firstName;
		this.surname = surname;
		setEmail(email);
		this.mobilePhone = mobilePhone;
		this.workPhone = workPhone;
		setPassword(password);
	}

	public User(String email, String password) {
		
		setEmail(email);
		setPassword(password);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		char [] charArray = email.toCharArray();
		boolean check = false;
		for (char c : charArray) {
			if (c == '@') {
				this.email = email;
				check = true;
				System.out.println("Email is valid for user " + getFirstName());
			} 
		}
		if (!check) {
			System.out.println("Email should contain @");
			throw new IllegalArgumentException();
		}
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
			if (password.length() >= 8 && password.length() <= 16) {
				System.out.println("Password is correct for user " + getFirstName());
				this.password = password;
			} else {
				System.out.println("Password should be minimum 8, max 16 symbols");
				throw new IllegalArgumentException();
			}	
	}

}
