package homework_1;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		System.out.println("Enter digits 1, 2 or 3:");
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();

		if (x == 1) {
			System.out.println("You entered 1");
		} else if (x == 2) {
			System.out.println("You entered 2");
		} else if (x == 3) {
			System.out.println("You entered 3");
		} else {
			System.out.println("Incorrect number");
		}

	}
}


