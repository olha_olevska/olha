
import java.text.DecimalFormat;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.swing.text.AbstractDocument.Content;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import net.bytebuddy.utility.RandomString;



public class Main {

	WebDriver driver = new ChromeDriver();
	static Random rand = new Random();
	static int[] phoneCodes = new int[] {50,63,66,67,68,91,92,93,97,96};
	static String emailFinal = "user" + (int)(Math.random()*1000) + "@gmail.com";
	static String pwd = "User" + (int)(Math.random()*1000) +"abc";
	static String lName = RandomString.make(5);
	static DecimalFormat df = new DecimalFormat("0000000");
	
	
	@BeforeClass
	public static void beforeClass() {
		final String path = String.format("%s/bin/chromedriver.exe", System.getProperty("user.dir"));
		System.setProperty("webdriver.chrome.driver", path);

	}
	

	@Test
	public void firstTest() {
		driver.get("https://user-data.hillel.it/html/registration.html");
		driver.manage().window().maximize();
		
		WebElement reg = driver.findElement(By.className("registration"));
		reg.click();

		WebElement firstName = driver.findElement(By.id("first_name"));
		firstName.sendKeys(RandomString.make(5));

		WebElement lastName = driver.findElement(By.id("last_name"));
		lastName.sendKeys(lName);

		WebElement WorkPhone = driver.findElement(By.id("field_work_phone"));
		int phone = rand.nextInt(99999);
		WorkPhone.sendKeys(Integer.toString(phone));

		WebElement MobilePhone = driver.findElement(By.id("field_phone"));
		int code = phoneCodes[rand.nextInt(phoneCodes.length)];
		int phoneTwo = rand.nextInt(9999999);
		MobilePhone.sendKeys("380" + code + df.format(phoneTwo));

		WebElement email = driver.findElement(By.id("field_email"));
		email.sendKeys(emailFinal);

		WebElement password = driver.findElement(By.id("field_password"));
		password.sendKeys(pwd);

		WebElement female = driver.findElement(By.id("female"));
		female.click();

		WebElement position = driver.findElement(By.id("position"));
		position.click();
		position.sendKeys("qa");
		position.click();

		WebElement regButton = driver.findElement(By.id("button_account"));
		regButton.click();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.close();
	
	}

	@Test
	public void secondTest() {
		driver.get("https://user-data.hillel.it/html/registration.html");
		driver.manage().window().maximize();
		
		WebElement emailLog = driver.findElement(By.id("email"));
		emailLog.sendKeys(emailFinal);

		WebElement passLog = driver.findElement(By.id("password"));
		passLog.sendKeys(pwd);

		WebElement logButton = driver.findElement(By.className("login_button"));
		logButton.click();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		WebElement emp = driver.findElement(By.linkText("Employees"));
		emp.click();

		WebElement search = driver.findElement(By.id("last_name"));
		search.sendKeys(lName);
		
		WebElement searchTwo = driver.findElement(By.id("position"));
		searchTwo.click();
		searchTwo.sendKeys("qa");
		
		WebElement searchThree = driver.findElement(By.id("gender"));
		searchThree.click();
		searchThree.sendKeys("female");

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		WebElement searchButton = driver.findElement(By.id("search"));
		searchButton.click();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		Assert.assertEquals(lName, lName);
		System.out.println("Assert passed");
		
		driver.close();
		
	}


}
